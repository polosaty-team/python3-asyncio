import asyncio
from contextlib import suppress


class Periodic:
    def __init__(self, func, time):
        self.func = func
        self.time = time
        self.is_started = False
        self._task = None

    async def start(self):
        if not self.is_started:
            self.is_started = True
            # Start task to call func periodically:
            self._task = asyncio.ensure_future(self._run())

    async def stop(self):
        if self.is_started:
            self.is_started = False
            # Stop task and await it stopped:
            self._task.cancel()
            with suppress(asyncio.CancelledError):
                await self._task

    async def _run(self):
        while True:
            await asyncio.sleep(self.time)
            try:
                # print('self.func:', self.func)
                if isinstance(self.func, asyncio.futures.Future):
                    print('future', self.func)
                    asyncio.ensure_future(self.func)
                else:
                    res = self.func()
                    # print('res:', res)
                    # print('is future res:', isinstance(res, asyncio.futures.Future))
                    # print('is future self.func:', isinstance(self.func, asyncio.futures.Future))
                    # print('is iscoroutinefunction self.func:', asyncio.iscoroutinefunction(self.func))
                    # print('is iscoroutinefunction res:', asyncio.iscoroutinefunction(res))
                    # print('is iscoroutine res:', asyncio.iscoroutine(res))
                    # print('is iscoroutine self.func:', asyncio.iscoroutine(self.func))
                    if asyncio.iscoroutine(res):
                        await res

            except Exception as ex:
                print(ex)


class Some(object):
    def __init__(self, loop):
        self._loop = loop
        asyncio.ensure_future(self.atask(1), loop=self._loop)  # fire and forget

        self._task1 = Periodic(lambda : self.task('2'), 2)
        asyncio.ensure_future(self._task1.start(), loop=self._loop)

        self._task2 = Periodic(lambda : self.atask('3'), 3)
        asyncio.ensure_future(self._task2.start(), loop=self._loop)

    def task(self, x='-'):
        print('task %s' % x)

    async def atask(self, x='-'):
        print('atask %s' % x)

    async def run(self):
        while True:
            print('run')
            await asyncio.sleep(1, loop=self._loop)


async def amain(**kwargs):
    # worker = await Some(loop=kwargs.get('loop', asyncio.get_event_loop()))
    worker = Some(loop=kwargs.get('loop', asyncio.get_event_loop()))
    await (worker.run())


def main(**kwargs):
    loop = asyncio.get_event_loop()
    try:
        asyncio.ensure_future(amain(**kwargs, loop=loop))
        loop.run_forever()
    except KeyboardInterrupt:
        loop.stop()


if __name__ == '__main__':
    main()
